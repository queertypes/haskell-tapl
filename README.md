# Types and Programming Languages Playground

Welcome!

This repository aims to contain Haskell implementations of the
languages and type systems specified in
[Types and Programming Languages](http://www.cis.upenn.edu/~bcpierce/tapl/).

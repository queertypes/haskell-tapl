# Untyped Arithmetic

## Terms

```
t ::=
    true
    false
    if t then t else t
    0
    succ t
    pred t
    iszero t

v ::=
    nv

nv ::=
    0
    succ nv
```

## Semantics

```
       t -> t'
  -----------------      (E-Succ)
  succ t -> succ t'

       pred 0
       ------            (E-PredZero)
          0

    pred (succ nv)
    --------------       (E-PredSucc)
          nv

       t -> t'
  -----------------      (E-Pred)
  pred t -> pred t'

      iszero 0
      --------           (E-IsZeroZero)
        true

  iszero (succ nv)       (E-IsZeroSucc)
  ----------------
        false

         t -> t'
  ---------------------  (E-IsZero)
  iszero t -> iszero t'
```

## Examples

```
> if false then 0 else 1
1
```

```
> iszero (pred (succ 0))
true
```

module Language.Untyped.Eval where

import Language.Untyped.Types

eval :: Term -> Maybe Term
eval t = case t of
  TIf TTrue t' _ -> Just t'
  TIf TFalse _ t' -> Just t'
  TIf b t1 t2 -> maybe Nothing (\b' -> eval $ TIf b' t1 t2) (eval b)
  TSucc t' -> eval t'
  TPred t' -> eval t'
  TPred TZero -> Just TZero
  TIsZero TZero -> Just TTrue
  TIsZero (TSucc t')
    | isNum t' -> Just TFalse
    | otherwise -> Nothing
  TIsZero t'
    | isNum t' -> Just TFalse
    | otherwise -> Nothing
  TTrue -> Just TTrue
  TFalse -> Just TFalse
  TZero -> Just TZero

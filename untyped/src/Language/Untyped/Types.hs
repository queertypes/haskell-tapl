module Language.Untyped.Types where

data Term
  = TTrue
  | TFalse
  | TIf Term Term Term
  | TZero
  | TSucc Term
  | TPred Term
  | TIsZero Term

isNum :: Term -> Bool
isNum t = case t of
  TZero -> True
  TSucc t' -> isNum t'
  _ -> False

isVal :: Term -> Bool
isVal t = case t of
  TTrue -> True
  TFalse -> True
  TZero -> True
  TSucc t' -> isNum t'
  _ -> False